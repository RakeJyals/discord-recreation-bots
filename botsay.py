import discord #for bot
import json #for filereading
import logging #for output of errors
import random #for choosing random url from list
from discord.ext import commands #for @botcommand

#Read json file
with open("../Genconfig") as config_file:
	config_json = json.load(config_file)
	token = config_json["token"]

#discord API variables, made bc they're classes (Jake ya dummy)
bot = commands.Bot(command_prefix="bot")

logging.basicConfig(level=logging.INFO)


#Dev vars, put in config later

@bot.command() #1 command per @
async def say(ctx, *, arg): #ctx-metadata, arg-message following command, only command is ppm
	await ctx.send(arg)

bot.run(token)
