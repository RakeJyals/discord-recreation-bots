import discord #for bot
import json #for filereading
import logging #for output of errors
import asyncio #enables sleep function
from discord.ext import commands #for @botcommand

#Read json file
with open("./config.json") as config_file:
	config_json = json.load(config_file)
	token = config_json["token"]

#discord API object initialization
bot = commands.Bot(command_prefix="bot_")

logging.basicConfig(level=logging.INFO)


#Dev vars


async def periodic_trigger():
        while True:
                await asyncio.sleep()

@bot.event
async def on_ready(): 
	print("ready")
#        bot.loop.create_task(periodic_trigger()) #Starts loop of periodic_trigger


@bot.command() #1 command per @
async def add(ctx, arg=None): #ctx-metadata, arg-message following command

bot.run(token)
