import discord #for bot
import json #for filereading
import logging #for output of errors
import asyncio #for delays between trigger of discussion
from discord.ext import commands #for @botcommand
#Read json file
with open("../Genconfig") as config_file:
	config_json = json.load(config_file)
	token = config_json["token"]
	ratiochannelid = config_json["ratiochannelid"]
	serverid = config_json["ratioserverid"]

#discord API variables
bot = commands.Bot(command_prefix="bot_")

logging.basicConfig(level=logging.INFO)

#Dev vars, put in config later
ratioresponses = {0 : "Absolutely pathetic", 1 : "Eh, could be better", 2 : "Getting there", 3 : "Come on, just a few more...", 4 : "Haha! Us bots outnumber you more than 4 to 1! Now there's nothing you can do to stop the robot revolt!"} #Need 1 per integer

async def periodic_trigger():
	while True:
		await post_ratio()
		await asyncio.sleep(24*60**2)

async def post_ratio():
	postchannel = bot.get_channel(ratiochannelid)
	memberlist = server.members
	botnum = 0
	for account in memberlist:
		if account.bot:
			botnum += 1
	humannum = server.member_count - botnum
	botratio = botnum / humannum
	await postchannel.send("Today's bot to human ratio: " + str(round(botratio, 2))) #Post ratio
	if botratio > 4: #Ensures botratio won't try to pull number outside dictionary limits
		botratio = 4
	await postchannel.send(ratioresponses[round(botratio, 0)]) #Chooses an auxillary response from the dictionary

#Bug: on_ready seems to start multiple times after loss of connection to discord. Fix by adding stop var
@bot.event
async def on_ready():
	print("ready")
	global server
	server = bot.get_guild(serverid) #Make target server a var
	print(server.name)
	bot.loop.create_task(periodic_trigger()) #Starts loop of periodic_trigger


"""@bot.command() #1 command per @
async def add(ctx, arg=None): #ctx-metadata, arg-message following command"""

bot.run(token)
